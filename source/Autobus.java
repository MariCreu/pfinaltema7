import javax.swing.JOptionPane;



/**
 * @author MariCreu
 *
 */
public class Autobus extends Vehiculo implements PuedeCircular {

	
	private String tipoRecorrido;
	private boolean esEscolar;
	/**
	 * Constructores
	 */
	public Autobus() {
		super();
		setTipoRecorrido("corto");
		setEsEscolar(false);
		setNumPlazas(55);
	}
	public Autobus(String matricula, String marca,String modelo, String color, float kilometros, int numPuertas, int numPlazas, String tipoRecorrido, boolean esEscolar){
		super( matricula,  marca,modelo,  color,  kilometros, numPuertas, numPlazas);
		setTipoRecorrido(tipoRecorrido);
		setEsEscolar(esEscolar);
	}
	/** M�todo sobreescrito de la interfaz PuedeCircular
	 * @see PuedeCircular#circular()
	 */
	@Override
	public String circular() {
		String circular;
		circular= "Esto es un autob�s y los autobuses pueden circular por carreteras, autov�as y autopistas";
		return circular;
	}

	/* M�todo sobreescrito de la clase Veh�culo
	 * @see Vehiculo#acelerar()
	 */
	@Override
	public String acelerar() {
		String acelerar;
		acelerar="El autob�s est� acelerando";
		return acelerar;
	}

	/* M�todo sobreescrito de la clase Veh�culo
	 * @see Vehiculo#arrancar()
	 */
	@Override
	public String arrancar() {
		String arrancar;
		arrancar="El autob�s est� arrancando";
		return arrancar;
	}

	/* M�todo sobreescrito de la clase Veh�culo
	 * @see Vehiculo#frenar()
	 */
	@Override
	public String frenar() {
		String frenar;
		frenar="El autob�s est� frenando";
		return frenar;
	}
	@Override
	public String toString(){
		String caracteristicas;
		String escolar;
		if (esEscolar){
			escolar="S�";
		}else{
			escolar="No";
		}
		
		caracteristicas= "\nLas Caracter�sticas de su " +getClass()+ " son:\n" + super.toString() + "\nTipo de Recorrido = "+getTipoRecorrido()+"\nEscolar = " + escolar;
		return caracteristicas;
	}
	/**
	 * Setters i Getters
	 * @return
	 */

	public String getTipoRecorrido() {
		return tipoRecorrido;
	}

	public void setTipoRecorrido(String tipoRecorrido) {
		this.tipoRecorrido = tipoRecorrido;
	}

	public boolean getEsEscolar() {
		return esEscolar;
	}

	public void setEsEscolar(boolean esEscolar) {
		this.esEscolar = esEscolar;
	}
	
	/**
	 * M�todos propios de autob�s
	 */
	public void abrirPuertas(){
		JOptionPane.showMessageDialog(null, "El autob�s est� abriendo sus puertas");
	}
	public void aparcar(){
		JOptionPane.showMessageDialog(null, "El autob�s est� aparcando");
	}
}
