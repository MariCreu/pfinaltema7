import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author MariCreu
 *
 */
public class Yate extends Vehiculo implements PuedeNavegar {

	private boolean tieneCocina;
	private int numMotores;
	private float metrosEslora;
	

	/**Constructores de Yate
	 * 
	 */
	public Yate() {
		super();
		setTieneCocina(false);
		setNumMotores(1);
		setMetrosEslora(10);
	}
	public Yate (String matricula, String marca, String modelo,String color, float kilometros, int numPuertas, int numPlazas, boolean tieneCocina,int numMotores,float metrosEslora){
		super( matricula,  marca,modelo,color, kilometros, numPuertas, numPlazas);
		setTieneCocina(tieneCocina);
		setNumMotores(numMotores);
		setMetrosEslora(metrosEslora);
	}

	/* M�todo heredado de la  interfaz PuedeNavegar
	 * @see PuedeNavegar#navegar()
	 */
	@Override
	public String navegar() {
		String navegar;
		navegar="Los yates navegan por el mar";
		return navegar;
	}

	/* M�todos heredados de veh�culo
	 * @see Vehiculo#acelerar()
	 */
	@Override
	public String acelerar() {
		String acelerar;
		acelerar="El yate est� acelerando";
		return acelerar;
	}

	
	@Override
	public String arrancar() {
		String arrancar;
		arrancar="El yate est� arrancando";
		return arrancar;
	}

	
	@Override
	public String frenar() {
		String frenar;
		frenar="El yateest� frenando";
		return frenar;
	}
	
	@Override
	public String toString(){
		String caracteristicas;
		String cocina;
		if (tieneCocina){
			cocina="S�";
		}else{
			cocina="No";
		}
		
		caracteristicas= "\nLas Caracter�sticas de su " +getClass()+ " son:\n" + super.toString() + "\nCocina = "+cocina+"\nMetros de eslora = " + getMetrosEslora()+"\nN�mero de motores = "+ getNumMotores();
		return caracteristicas;
	}
	
/**Setters i Getters
 * 
 * @return
 */
	public boolean getTieneCocina() {
		return tieneCocina;
	}

	public void setTieneCocina(boolean tieneCocina) {
		this.tieneCocina = tieneCocina;
	}

	public int getNumMotores() {
		return numMotores;
	}

	public void setNumMotores(int numMotores) {
		this.numMotores = numMotores;
	}

	public float getMetrosEslora() {
		return metrosEslora;
	}

	public void setMetrosEslora(float metrosEslora) {
		this.metrosEslora = metrosEslora;
	}
	/**M�todos propios de Yate
	 * 
	 */
	public void zarpar(){
		JOptionPane.showMessageDialog(null, "El yate est� zarpando");
	}
	public void atracar(){
		JOptionPane.showMessageDialog(null, "El yate est� atracando en el puerto");
	}
}
