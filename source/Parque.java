import java.util.ArrayList;

import javax.swing.JOptionPane;

public class Parque {

	
	/**
	 * Clase principal que contiene el main
	 */
	
	public static void main(String[] args) {
		int opcion = 0;
	
		 String matricula="";
		 String marca="";
		 String modelo="";
		String color="";
		 float km=0;
		 int puertas=3;
		 int plazas=5;
		 int numAirbags=0;
		 boolean techo_sol=false;
		 boolean radio=false;
		 boolean ser_escolar=false;
		 String tipoRecorrido="";
		 int potencia=50;
		boolean maletero=false;
		int kg=0;
		int motores=1;
		float metros=2;
		boolean cocina=false;
		
		 ArrayList<Vehiculo> vehiculo = new ArrayList<Vehiculo>();
		
		
		
		do{
			try{
				String a= JOptionPane.showInputDialog("Elija la opci�n a realizar:\n1. Crear un coche\n2. Crear un autob�s\n3. Crear una motocicleta\n4. Crear una avioneta\n"
						+ "5. Crear un yate\n6. Mostrar las caracter�sticas de todos los veh�culos del parque\n7. Elegir un veh�culo\n8. Vender un veh�culo\n9. Salir del programa");
				opcion=Integer.parseInt(a);
				switch (opcion){
				case 1:
					if(Vehiculo.getNum_vehiculos()>=Vehiculo.MAX_vehiculos){
						JOptionPane.showMessageDialog(null, "No puede crear m�s veh�culos");
					}
					else{
						int datos=0;
					
							
								String b=JOptionPane.showInputDialog("Elija una opci�n:\n1. Crear un coche sin datos\n2. Crear un coche con datos");
								datos=Integer.parseInt(b);
						
								
						
						
						if(datos==1){
							Vehiculo co=new Coche();
							vehiculo.add(co);
							JOptionPane.showMessageDialog(null,co.toString());
						}
						else {
							if(datos==2){
							JOptionPane.showMessageDialog(null, "Introduzca los datos del coche que va a ser fabricado");
						
							matricula=JOptionPane.showInputDialog("Introduzca la matr�cula");
							color=JOptionPane.showInputDialog("Introduzca el color");
							 marca=JOptionPane.showInputDialog("Introduzca la marca");
							 modelo=JOptionPane.showInputDialog("Introduzca el modelo");
							
							try{
								String kilometros=JOptionPane.showInputDialog("Introduzca los kilometros");
								km=Float.parseFloat(kilometros);
							}
							catch (Exception e){
								JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� el coche con 0 km ");
								
								
							}
							
							try{
								String numPuertas=JOptionPane.showInputDialog("Introduzca el n�mero de puertas");
								puertas=Integer.parseInt(numPuertas);
								if( puertas > 5 ||puertas<0){
									puertas=3;
									JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� el coche con 3 puertas por defecto ");
								}
							}
							catch (Exception e){
								JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� el coche con 3 puertas por defecto ");
							}		
							
							
							try{
								String numPlazas=JOptionPane.showInputDialog("Introduzca el n�mero de plazas");
								plazas=Integer.parseInt(numPlazas);
								if(plazas>10 || plazas<0){
									plazas=5;
									JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� el coche con 5 plazas por defecto ");
								}
							}
							catch (Exception e){
								JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� el coche con 5 plazas por defecto ");
							}		
							
							
							try{
								String airbags=JOptionPane.showInputDialog("Introduzca el n�mero de airbags");
								numAirbags=Integer.parseInt(airbags);
								if(numAirbags >10){
									numAirbags=0;
									JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� el coche con 0 airbags por defecto ");
								}
							}
							catch (Exception e){
								JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� el coche con 0 airbags por defecto ");
							}		
								
							
							String techo=JOptionPane.showInputDialog("Introduzca Si o No si desea techo solar");
							if (techo.equals("si")|| techo.equals("SI")||techo.equals("Si")){
								techo_sol=true;
							}else{
								if(techo.equals("no")||techo.equals("NO")||techo.equals("No")){
									techo_sol=false;
									}
									else{
										JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� el coche sin techo solar por defecto ");	
									}
							}
								
							
							String rad=JOptionPane.showInputDialog("Introduzca Si o No si desea radio");
							
							if (rad.equals("si")||rad.equals("SI")||rad.equals("Si")){
								radio=true;
							}else{
								if(rad.equals("no")||rad.equals("NO")||rad.equals("No")){
									radio=false;
								}
							
							else{
								JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� el coche sin radio por defecto ");	
							}
							}
							
							
							Vehiculo co=new Coche(matricula, marca, modelo,  color, km, puertas, plazas, numAirbags,techo_sol,radio);
							vehiculo.add(co);
							JOptionPane.showMessageDialog(null, co.toString());
							break;
							}
							
					
						else{
							JOptionPane.showMessageDialog(null, "La opci�n introducida no es v�lida");
						}
						}}	
						break;
				case 2:
					if(Vehiculo.getNum_vehiculos()>=Vehiculo.MAX_vehiculos){
						JOptionPane.showMessageDialog(null, "No puede crear m�s veh�culos");
					}
					else{
						String b=JOptionPane.showInputDialog("Elija una opci�n:\n1. Crear un autob�s sin datos\n2. Crear un autob�s con datos");
						int datos=Integer.parseInt(b);
						if(datos==1){
							Vehiculo au=new Autobus();
							vehiculo.add(au);
							JOptionPane.showMessageDialog(null, au.toString());
						}
						else {
					if(datos==2){
					JOptionPane.showMessageDialog(null, "Introduzca los datos del autob�s que va a ser fabricado");
				
					matricula=JOptionPane.showInputDialog("Introduzca la matr�cula");
					color=JOptionPane.showInputDialog("Introduzca el color");
					 marca=JOptionPane.showInputDialog("Introduzca la marca");
					 modelo=JOptionPane.showInputDialog("Introduzca el modelo");
					
					try{
						String kilometros=JOptionPane.showInputDialog("Introduzca los kilometros");
						km=Float.parseFloat(kilometros);
						if(km<0){
							JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� el autob�s con 0 km ");
						}
					}
					catch (Exception e){
						JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� el autob�s con 0 km ");
						
						
					}
					
					try{
						String numPuertas=JOptionPane.showInputDialog("Introduzca el n�mero de puertas");
						puertas=Integer.parseInt(numPuertas);
						if(puertas>5||puertas<0){
							puertas=3;
							JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� el autob�s con 3 puertas por defecto ");
						}
					}
					catch (Exception e){
						JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� el autob�s con 3 puertas por defecto ");
					}		
					
					
					try{
						String numPlazas=JOptionPane.showInputDialog("Introduzca el n�mero de plazas");
						plazas=Integer.parseInt(numPlazas);
						if(plazas>100 || plazas<1){
							plazas=55;
							JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� el autob�s con 55 plazas por defecto ");
						}
					}
					catch (Exception e){
						plazas=55;
						JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� el autob�s con 55 plazas por defecto ");
					}		
					
					String escolar=JOptionPane.showInputDialog("Introduzca Si o No si el autob�s es escolar");
					if (escolar.equals("si")||escolar.equals("SI")||escolar.equals("Si")){
						ser_escolar=true;
					}else{
						if(escolar.equals("no")||escolar.equals("NO")||escolar.equals("No")){
							ser_escolar=false;
							}
							else{
								JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� el autob�s sin ser escolar por defecto ");	
							}
						}
						
					
					 tipoRecorrido=JOptionPane.showInputDialog("Introduzca el tipo de recorrido del autob�s");
					
					
					
					Vehiculo co=new Autobus(matricula, marca,modelo,  color, km, puertas, plazas,tipoRecorrido, ser_escolar);
					vehiculo.add(co);
					JOptionPane.showMessageDialog(null, co.toString());
					break;
					}
					
			
					else{
							JOptionPane.showMessageDialog(null, "La opci�n introducida no es v�lida");
						}
						}}	
						
						
					break;
				case 3:
					if(Vehiculo.getNum_vehiculos()>=Vehiculo.MAX_vehiculos){
						JOptionPane.showMessageDialog(null, "No puede crear m�s veh�culos");
					}
					else{
						int datos=0;
						String b=JOptionPane.showInputDialog("Elija una opci�n:\n1. Crear una motocicleta sin datos\n2. Crear una motocicleta con datos");
						datos=Integer.parseInt(b);
				
						if(datos==1){
							Vehiculo moto=new Motocicleta();
							vehiculo.add(moto);
							JOptionPane.showMessageDialog(null, moto.toString());
						}
						else {
							if(datos==2){
								JOptionPane.showMessageDialog(null, "Introduzca los datos de la motocicleta que va a ser fabricada");
								
								matricula=JOptionPane.showInputDialog("Introduzca la matr�cula");
								color=JOptionPane.showInputDialog("Introduzca el color");
								 marca=JOptionPane.showInputDialog("Introduzca la marca");
								 modelo=JOptionPane.showInputDialog("Introduzca el modelo");
								 
								try{
									String kilometros=JOptionPane.showInputDialog("Introduzca los kilometros");
									km=Float.parseFloat(kilometros);
									if(km<0){
										km=0;
										JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� la motocicleta con 0 km ");
										
									}
								}
								catch (Exception e){
									JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� la motocicleta con 0 km ");
									
									
								}
								
								try{
									String numPuertas=JOptionPane.showInputDialog("Introduzca el n�mero de puertas");
									puertas=Integer.parseInt(numPuertas);
									if(puertas!=0){
										JOptionPane.showMessageDialog(null, "Las motocicletas no tienen puertas por lo que se tomar� el valor 0 para el n�mero de puertas");
										puertas=0;
									}
								}
								catch (Exception e){
									JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� la motocicleta con 0 puertas por defecto ");
									puertas=0;
								}		
								
								
								try{
									String numPlazas=JOptionPane.showInputDialog("Introduzca el n�mero de plazas");
									plazas=Integer.parseInt(numPlazas);
									if(plazas<0||plazas>2){
										JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� la motocicleta con 2 plazas por defecto ");
										plazas=2;
									}
								}
								catch (Exception e){
									JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� la motocicleta con 2 plazas por defecto ");
									plazas=2;
								}		
								
								try{
									String pot=JOptionPane.showInputDialog("Introduzca la potencia de la motocicleta");
									potencia=Integer.parseInt(pot);
									if(potencia<0||potencia>10000){
										potencia=50;
										JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� la motocicleta con 50 cv por defecto ");
									}
								}
								catch (Exception e){
									JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� la motocicleta con 50 cv por defecto ");
									
								}		
								String mal=JOptionPane.showInputDialog("Introduzca Si o No si la motocicleta tiene maletero");
								if (mal.equals("si")||mal.equals("SI")||mal.equals("Si")){
									maletero=true;
								}else{
									if(mal.equals("no")||mal.equals("NO")||mal.equals("No")){
										maletero=false;
										}
										else{
											JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� la motocicleta sin maletero por defecto ");	
										}
									}
									
								
								Vehiculo co=new Motocicleta(matricula, marca,modelo,  color, km, puertas, plazas,potencia,maletero);
								vehiculo.add(co);	
								JOptionPane.showMessageDialog(null, co.toString());
								
								
							}else{
								JOptionPane.showMessageDialog(null, "La opci�n introducida no es v�lida");
							}
							}
						
						
						
						
					}
					
					break;
				case 4:
					if(Vehiculo.getNum_vehiculos()>=Vehiculo.MAX_vehiculos){
						JOptionPane.showMessageDialog(null, "No puede crear m�s veh�culos");
					}
					else{
						int datos=0;
						String b=JOptionPane.showInputDialog("Elija una opci�n:\n1. Crear una avioneta sin datos\n2. Crear una avioneta con datos");
						datos=Integer.parseInt(b);
				
						if(datos==1){
							Vehiculo avio=new Avioneta();
							vehiculo.add(avio);
							JOptionPane.showMessageDialog(null, avio.toString());
						}
						else {
							if(datos==2){
								JOptionPane.showMessageDialog(null, "Introduzca los datos de la avioneta que va a ser fabricada");
								
								matricula=JOptionPane.showInputDialog("Introduzca la matr�cula");
								color=JOptionPane.showInputDialog("Introduzca el color");
								 marca=JOptionPane.showInputDialog("Introduzca la marca");
								 modelo=JOptionPane.showInputDialog("Introduzca el modelo");
								 
								try{
									String kilometros=JOptionPane.showInputDialog("Introduzca los kilometros");
									km=Float.parseFloat(kilometros);
									if(km<0){
										km=0;
										JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� la avioneta con 0 km ");
									}
								}
								catch (Exception e){
									JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� la avioneta con 0 km ");
									km=0;
									
								}
								
								try{
									String numPuertas=JOptionPane.showInputDialog("Introduzca el n�mero de puertas");
									puertas=Integer.parseInt(numPuertas);
									if(puertas<1||puertas>10){
										JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� la avioneta con 3 puertas por defecto ");
										puertas=3;
									}
								}
								catch (Exception e){
									JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� la avioneta con 3 puertas por defecto ");
									puertas=3;
								}		
								
								
								try{
									String numPlazas=JOptionPane.showInputDialog("Introduzca el n�mero de plazas");
									plazas=Integer.parseInt(numPlazas);
									if(plazas<1||plazas>100){
										plazas=10;
										JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� la avioneta con 10 plazas por defecto ");
									}
								}
								catch (Exception e){
									JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� la avioneta con 10 plazas por defecto ");
									plazas=10;
								}		
								
								try{
									String kilos=JOptionPane.showInputDialog("Introduzca los kg que se pueden llevar como m�ximo");
									kg = Integer.parseInt(kilos);
									if(kg<0||kg>100){
										kg=0;
										JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� llevar�n 0 kg por defecto ");
									}
								}
								catch (Exception e){
									JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� llevar�n 0 kg por defecto ");
									kg=0;
									
								}		
								String aeropuerto=JOptionPane.showInputDialog("Introduzca el nombre del aeropuerto");
								
								
								Vehiculo avio=new Avioneta(matricula, marca, modelo, color, km, puertas, plazas,aeropuerto,kg);
								vehiculo.add(avio);	
								JOptionPane.showMessageDialog(null, avio.toString());
								
								
							}else{
								JOptionPane.showMessageDialog(null, "La opci�n introducida no es v�lida");
							}}
						
						
					}
					break;
				case 5:
					if(Vehiculo.getNum_vehiculos()>=Vehiculo.MAX_vehiculos){
						JOptionPane.showMessageDialog(null, "No puede crear m�s veh�culos");
					}
					else{
						int datos=0;
						String b=JOptionPane.showInputDialog("Elija una opci�n:\n1. Crear un yate sin datos\n2. Crear un yate con datos");
						datos=Integer.parseInt(b);
				
						if(datos==1){
							Vehiculo ya=new Yate();
							vehiculo.add(ya);
							JOptionPane.showMessageDialog(null, ya.toString());
						}
						else {
							if(datos==2){
								JOptionPane.showMessageDialog(null, "Introduzca los datos del yate que va a ser fabricado");
								
								matricula=JOptionPane.showInputDialog("Introduzca la matr�cula");
								color=JOptionPane.showInputDialog("Introduzca el color");
								 marca=JOptionPane.showInputDialog("Introduzca la marca");
								 modelo=JOptionPane.showInputDialog("Introduzca el modelo");
								 
								try{
									String kilometros=JOptionPane.showInputDialog("Introduzca los kilometros");
									km=Float.parseFloat(kilometros);
									if(km<0){
										km=0;
										JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� el yate con 0 km ");
									}
								}
								catch (Exception e){
									JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� el yate con 0 km ");
									km=0;
									
								}
								
								try{
									String numPuertas=JOptionPane.showInputDialog("Introduzca el n�mero de puertas");
									puertas=Integer.parseInt(numPuertas);
									if(puertas<0||puertas>10){
										puertas=3;
										JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� el yate con 3 puertas por defecto ");
									}
								}
								catch (Exception e){
									JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� el yate con 3 puertas por defecto ");
									puertas=3;
								}		
								
								
								try{
									String numPlazas=JOptionPane.showInputDialog("Introduzca el n�mero de plazas");
									plazas=Integer.parseInt(numPlazas);
									if(plazas<1||plazas>1000){
										plazas=10;
										JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� el yate con 10 plazas por defecto ");
									}
								}
								catch (Exception e){
									JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� el yate con 10 plazas por defecto ");
									plazas=10;
								}		
								
								try{
									String mot=JOptionPane.showInputDialog("Introduzca la cantidad de motores");
									motores = Integer.parseInt(mot);
									if(motores<1||motores>10){
										motores=1;
										JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� 1 motor por defecto ");
									}
								}
								catch (Exception e){
									JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� 1 motor por defecto ");
									
								}		
								try{
									String met=JOptionPane.showInputDialog("Introduzca el n�mero de metros de eslora");
									metros =Float.parseFloat(met);
									if(metros<1||metros>1000){
										metros=1;
										JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� 1 metro por defecto");
									}
								}
								catch (Exception e){
									JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� 1 metro por defecto");
									metros=1;
								}		
								
								String cocin=JOptionPane.showInputDialog("Introduzca Si o No si el yate tiene cocina");
								if (cocin.equals("si")||cocin.equals("SI")||cocin.equals("Si")){
									cocina=true;
								}else{
									if(cocin.equals("no")||cocin.equals("NO")||cocin.equals("No")){
										cocina=false;
										}
										else{
											JOptionPane.showMessageDialog(null, "El dato introducido no es correcto\n Se crear� el yate  sin cocina por defecto ");	
										}
									}
								
								
								Vehiculo ya=new Yate(matricula, marca, modelo,  color, km, puertas, plazas,cocina,motores,metros);
								vehiculo.add(ya);	
								JOptionPane.showMessageDialog(null, ya.toString());
								
								
							}else{
								JOptionPane.showMessageDialog(null, "La opci�n introducida no es v�lida");
							}}
						
						
						
						
						
						
					
					}
					break;
				case 6:
					if(vehiculo.isEmpty()){
						JOptionPane.showMessageDialog(null, "Todav�a no hay ning�n veh�culo en el parque");
					}else{
						mostrarArray(vehiculo);
					}
					break;
					
				case 7:
					if(vehiculo.isEmpty()){
						JOptionPane.showMessageDialog(null, "Todav�a no hay ning�n veh�culo en el parque");
					}else{
							
							matricula= JOptionPane.showInputDialog("Introduzca la matr�cula del veh�culo que desea escoger");
							int tipo=0;
							int indice=0;
							for(int x=0;x<vehiculo.size();x++){
								Vehiculo o= vehiculo.get(x);
								if(matricula.equals(o.getMatricula())){
									tipo=tipoVehiculo(o);
									indice=x;
								}
							}
							
							if (tipo==0){
								JOptionPane.showMessageDialog(null, "La matr�cula introducida no pertenece a ning�n veh�culo del parque");
							}else{
								if(tipo==1){
									
										Vehiculo o= vehiculo.get(indice);
										Coche a1=(Coche) o;
										String opcion_coche=JOptionPane.showInputDialog("Introduzca la opci�n deseada:\n1.Poner en marcha el coche\n2.Tunear el coche\n3.Aparcar el coche");
										if(opcion_coche.equals("1")){
											JOptionPane.showMessageDialog(null, a1.arrancar()+"\n"+a1.acelerar()+"\n"+a1.frenar());
										}else{
											if(opcion_coche.equals("2")){
												String color_tunear=JOptionPane.showInputDialog("Introduzca el color del que quiere pintar el coche");
												a1.tunear(color_tunear);
											}else{
												if(opcion_coche.equals("3")){
													a1.aparcar();
												}else{
													JOptionPane.showMessageDialog(null, "La opci�n introducida no es v�lida");
												}
											}
										}
										
								}
								if(tipo==2){
									Vehiculo o= vehiculo.get(indice);
									Autobus a1=(Autobus) o;
									String opcion_bus=JOptionPane.showInputDialog("Introduzca la opci�n deseada:\n1.Poner en marcha el autob�s\n2.Abrir las puertas\n3.Aparcar el autob�s");
									if(opcion_bus.equals("1")){
										JOptionPane.showMessageDialog(null, a1.arrancar()+"\n"+a1.acelerar()+"\n"+a1.frenar());
									}else{
										if(opcion_bus.equals("2")){
											a1.abrirPuertas();
										}else{
											if(opcion_bus.equals("3")){
												a1.aparcar();
											}else{
												JOptionPane.showMessageDialog(null, "La opci�n introducida no es v�lida");
											}
										}
									}
									
								}
								if(tipo==3){
									Vehiculo o= vehiculo.get(indice);
									Motocicleta a1=(Motocicleta) o;
									String opcion_moto=JOptionPane.showInputDialog("Introduzca la opci�n deseada:\n1.Poner en marcha la motocicleta\n2.Brincar con la motocicleta\n3.Aparcar la motocicleta");
									if(opcion_moto.equals("1")){
										JOptionPane.showMessageDialog(null, a1.arrancar()+"\n"+a1.acelerar()+"\n"+a1.frenar());
									}else{
										if(opcion_moto.equals("2")){
											a1.brincar();
										}else{
											if(opcion_moto.equals("3")){
												a1.aparcar();
											}else{
												JOptionPane.showMessageDialog(null, "La opci�n introducida no es v�lida");
											}
										}
									}
									
								}
								if(tipo==4){
									Vehiculo o= vehiculo.get(indice);
									Avioneta a1=(Avioneta) o;
									String opcion_avio=JOptionPane.showInputDialog("Introduzca la opci�n deseada:\n1.Poner en marcha la avioneta\n2.Despegar\n3.Aterrizar");
									if(opcion_avio.equals("1")){
										JOptionPane.showMessageDialog(null, a1.arrancar()+"\n"+a1.acelerar()+"\n"+a1.frenar());
									}else{
										if(opcion_avio.equals("2")){
											a1.despegar();
										}else{
											if(opcion_avio.equals("3")){
												a1.arrancar();
											}else{
												JOptionPane.showMessageDialog(null, "La opci�n introducida no es v�lida");
											}
										}
									}
									
								}
								if(tipo==5){
									Vehiculo o= vehiculo.get(indice);
									Yate a1=(Yate) o;
									String opcion_yat=JOptionPane.showInputDialog("Introduzca la opci�n deseada:\n1.Poner en marcha el yate\n2.Zarpar\n3.Atracar");
									if(opcion_yat.equals("1")){
										JOptionPane.showMessageDialog(null, a1.arrancar()+"\n"+a1.acelerar()+"\n"+a1.frenar());
									}else{
										if(opcion_yat.equals("2")){
											a1.zarpar();
										}else{
											if(opcion_yat.equals("3")){
												a1.atracar();
											}else{
												JOptionPane.showMessageDialog(null, "La opci�n introducida no es v�lida");
											}
										}
									}
									
									
								}
		
								}
					}
							
					break;
				case 8:
					if(vehiculo.isEmpty()){
						JOptionPane.showMessageDialog(null, "Todav�a no hay ning�n veh�culo en el parque");
					}else{
							
							matricula= JOptionPane.showInputDialog("Introduzca la matr�cula del veh�culo que desea vender");
							
							int indice=0;
							for(int x=0;x<vehiculo.size();x++){
								Vehiculo o= vehiculo.get(x);
								if(matricula.equals(o.getMatricula())){
									indice=vehiculo.indexOf(o);
								}
							}
							if(indice==-1){
								JOptionPane.showMessageDialog(null, "La matr�cula introducida no pertenece a ning�n veh�culo del parque");
							} else{
								vehiculo.remove(indice);
								JOptionPane.showMessageDialog(null, "El veh�culo con matr�cula "+matricula+" acaba de ser vendido y ya no se encuentra en el parque" );
								
							}
					
					}
					break;
					
				case 9:
					JOptionPane.showMessageDialog(null, "Hasta pronto");
					break;
				
					
					
				}
			}
			catch(Exception e){
				JOptionPane.showMessageDialog(null, "La opci�n introducida no es v�lida");
			}
		} while(opcion!=9);
	
		
		
		
		}
	
	/**
	 * M�todo que recorre el arraylist y muestra toda  la informaci�n
	 * @param vehiculo
	 */
	public static void mostrarArray(ArrayList<Vehiculo> vehiculo){
		
		for(int x=0;x<vehiculo.size();x++){
			JOptionPane.showMessageDialog(null, "El veh�culo n�mero "+ (x+1) + " es:");
			Vehiculo o= vehiculo.get(x);
			int tipo=tipoVehiculo(o);
			if(tipo==1){
				Coche a1=(Coche) o;
				JOptionPane.showMessageDialog(null, o.toString());
				String circular=a1.circular();
				JOptionPane.showMessageDialog(null, circular);
			}else{
				if(tipo==2){
					Autobus a1= (Autobus)o;
					JOptionPane.showMessageDialog(null, o.toString());
					String circular=a1.circular();
					JOptionPane.showMessageDialog(null, circular);
				}else{
					if(tipo==3){
						Motocicleta a1= (Motocicleta) o;
						JOptionPane.showMessageDialog(null, o.toString());
						String circular=a1.circular();
						JOptionPane.showMessageDialog(null, circular);
					}else{
						if(tipo==4){
							Avioneta a1=(Avioneta) o;
							JOptionPane.showMessageDialog(null, o.toString());
							String circular=a1.circular();
							String volar=a1.volar();
							JOptionPane.showMessageDialog(null, circular + volar);
						}else{
							if(tipo==5){
								Yate a1= (Yate) o;
								JOptionPane.showMessageDialog(null, o.toString());
								String navegar=a1.navegar();
								JOptionPane.showMessageDialog(null, navegar);
							}else{
								JOptionPane.showMessageDialog(null, "El objeto no es ning�n tipo de veh�culo");
							}
						}
					}
				}
			}
			
			
		}
		
		
		
	}
	/**
	 * M�todo para hacer el casting
	 * @param v
	 * @return
	 */
	public static int tipoVehiculo(Vehiculo v){
		int tipo=0;
		if(v instanceof Coche){
			tipo=1;
		}else{
			if(v instanceof Autobus){
				tipo=2;
			}else{
				if(v instanceof Motocicleta){
					tipo=3;
				}else{
					if(v instanceof Avioneta){
						tipo=4;
					}else{
						if(v instanceof Yate){
							tipo=5;
						}else{
							tipo=-1;
						}
					}
				}
			}
		}
	return tipo;
	}
	
	}
	


