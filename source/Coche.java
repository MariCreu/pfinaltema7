import javax.swing.JOptionPane;

public class Coche extends Vehiculo implements PuedeCircular {
	private int numAirbags;
	private boolean techoSolar;
	private boolean tieneRadio;
	
	/**
	 * Constructors
	 */
	public Coche() {
		super();
		setNumAirbags(0);
		setTechoSolar(false);
		setTieneRadio(false);
		
	}
	public Coche(String matricula, String marca,String modelo, String color, float kilometros, int numPuertas, int numPlazas, int numAirbags, boolean techoSolar, boolean tieneRadio){
		super( matricula,  marca,modelo,  color, kilometros, numPuertas, numPlazas);
		setNumAirbags(numAirbags);
		setTechoSolar(techoSolar);
		setTieneRadio(tieneRadio);
	}
	
	/**
	 * Setters i Getters
	 */
	public int getNumAirbags() {
		return numAirbags;
	}

	public void setNumAirbags(int numAirbags) {
		if(numAirbags<0||numAirbags>10){
			numAirbags=10;
		}
		this.numAirbags = numAirbags;
	}

	public boolean getTechoSolar() {
		return techoSolar;
	}

	public void setTechoSolar(boolean techoSolar) {
		this.techoSolar = techoSolar;
	}

	public boolean getTieneRadio() {
		return tieneRadio;
	}

	public void setTieneRadio(boolean tieneRadio) {
		this.tieneRadio = tieneRadio;
	}
	
	/**
	 * Metodos Sobreescritos
	 */
	@Override
	public String circular() {
		String circular;
		circular= "Esto es un coche y los coches pueden circular por carreteras, autov�as y autopistas";
		return circular;
	}

	@Override
	public String acelerar() {
		String acelerar;
		acelerar="El coche est� acelerando";
		return acelerar;
	}

	@Override
	public String arrancar() {
		String acelerar;
		acelerar="El coche est� acelerando";
		return acelerar;
	}

	@Override
	public String frenar() {
		String frenar;
		frenar="El coche est� frenando";
		return frenar;
	}
	@Override
	public String toString(){
		String caracteristicas;
		String techo,radio;
		if (techoSolar){
			techo="S�";
		}else{
			techo="No";
		}
		if (tieneRadio){
			radio="S�";
		}
		else {
			radio="No";
		}
		caracteristicas= "\nLas Caracter�sticas de su " +getClass()+ " son:\n" + super.toString() + "\nN�mero de Airbags = "+getNumAirbags()+"\nTecho Solar = " + techo + "\nRadio = "+radio;
		return caracteristicas;
	}
	
	/**
	 * Metodos propios de la clase Coche
	 */

	public void tunear(String color){
		
		super.setKilometros(0);
		if ( techoSolar==false){
			this.setTechoSolar(true);
		}
		
		super.setColor(color);
		JOptionPane.showMessageDialog(null, "Su coche tiene inicializados los km, dispone de techo solar, y ha sido pintado de color "+ super.getColor());;
	}
	public void aparcar(){
		JOptionPane.showMessageDialog(null, "El coche est� aparcando");
	}
}
