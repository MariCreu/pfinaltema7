/**
 * 
 */

/**
 * @author MariCreu
 *
 */
public abstract class Vehiculo {

	/**
	 * variables de instancia de la clase Veh�culo
	 */
	final static int MAX_vehiculos =5;
	private static int num_vehiculos=0;
	private String matricula;
	private String marca;
	private String modelo;
	private String color;
	private float kilometros;
	private int numPuertas;
	private int numPlazas;
	
	/** Constructores con par�metros y sin par�metros
	 * 
	 */
	public Vehiculo() {
	setColor("blanco");	
	setKilometros(0);
	setMatricula(matAleatoria());
	setMarca("Mazda");
	setModelo("CX3");
	setNumPuertas(3);
	setNumPlazas(2);
	
	num_vehiculos=getNum_vehiculos() + 1;
	}
	public Vehiculo(String matricula, String marca,String modelo, String color, float kilometros, int numPuertas, int numPlazas){
		this();
		setMatricula(matricula);
		setMarca(marca);
		setModelo(modelo);
		setColor(color);
		setKilometros(kilometros);
		setNumPuertas(numPuertas);
		setNumPlazas(numPlazas);
		
	}

	/**
	 * Setters i Getters
	 * @return
	 */
	
	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public float getKilometros() {
		return kilometros;
	}

	public void setKilometros(float kilometros) {
		if(kilometros>=0){
		this.kilometros = kilometros;
	}}

	public int getNumPuertas() {
		return numPuertas;
	}

	public void setNumPuertas(int numPuertas) {
		if(numPuertas>0 &&numPuertas<6){
		this.numPuertas = numPuertas;
	}}

	public int getNumPlazas() {
		return numPlazas;
	}

	public void setNumPlazas(int numPlazas) {
		if(numPlazas>0 && numPlazas<100){
		this.numPlazas = numPlazas;
	}}

	public static int getNum_vehiculos() {
		return num_vehiculos;
	}

	
	/**
	 * M�todos abstractos
	 * @return
	 */
	public abstract String acelerar();
	public abstract String arrancar();
	public abstract String frenar();
	
	/**
	 * M�todos sobreescritos
	 */
	@Override
	public String toString(){
		String caracteristicas = "Matr�cula "+ getMatricula()+ "\n"
				+ "Marca = " + getMarca()+"\nModelo = "+modelo+ "\nColor = "+getColor()+ "\nKilometros = "+getKilometros()+"\nN�mero de puertas = "
				+ getNumPuertas()+"\nN�mero de plazas = "+ getNumPlazas();
		return caracteristicas;
		
	}
	
	/**Matricula aleatoria
	 * 
	 * @return
	 */
	public static String matAleatoria(){
	    	int randomMat=(int)(Math.random()*100000);
	    	String matricula= Integer.toString(randomMat);
	    	while(matricula.length() < 5){
     		matricula="0"+matricula;
     	}
	    	 return matricula;
	    }
}