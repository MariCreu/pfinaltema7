import javax.swing.JOptionPane;


/**
 * @author MariCreu
 *
 */
public class Avioneta extends Vehiculo implements PuedeVolar, PuedeCircular {

	
	private String aeropuerto;
	private int maxKg;
	/**
	 * Constructores de Avioneta
	 */
	public Avioneta() {
		super();
		setAeropuerto("Manises");
		setMaxKg(10);
	}
	public Avioneta(String matricula, String marca,String modelo, String color, float kilometros, int numPuertas, int numPlazas, String aeropuerto,int maxKg){
		super(matricula, marca,modelo, color,  kilometros, numPuertas, numPlazas);
		setAeropuerto(aeropuerto);
		setMaxKg(maxKg);
	}

	/* M�todos heredados de las interfaces que implementa
	 * @see PuedeVolar#volar()
	 */
	@Override
	public String volar() {
		String volar;
		volar= "Los aviones y las avionetas pueden volar y su circuito es a�reo";
		return volar;
	}     
  @Override()
  public String circular() {
    String circular;
    circular="Las avionetas empiezan circulando por la pista aunque...";
    return circular;
  
  }
  
  

	/*M�todos heredados de Veh�culo
	 * @see Vehiculo#acelerar()
	 */
	@Override
	public String acelerar() {
		String acelerar;
		acelerar="La avioneta est� acelerando";
		return acelerar;
	}

	
	@Override
	public String arrancar() {
		String arrancar;
		arrancar="La avioneta est� arrancando";
		return arrancar;
	}

	
	@Override
	public String frenar() {
		String frenar;
		frenar="La avioneta est� frenando";
		return frenar;
	}
	
	@Override
	public String toString(){
		String caracteristicas;
		
		caracteristicas= "\nLas Caracter�sticas de su " +getClass()+ " son:\n" + super.toString() + "\nAeropuerto = "+getAeropuerto()+"\nM�ximo de kg= " + getMaxKg();
		return caracteristicas;
	}
	/**
	 * Setters i Getters
	 * @return
	 */
	
	public String getAeropuerto() {
		return aeropuerto;
	}

	public void setAeropuerto(String aeropuerto) {
		this.aeropuerto = aeropuerto;
	}

	public int getMaxKg() {
		return maxKg;
	}

	public void setMaxKg(int maxKg) {
		this.maxKg = maxKg;
	}
	/**
	 * M�todos propios de Avioneta
	 * @return
	 */
	public void despegar(){
		JOptionPane.showMessageDialog(null, "La avioneta est� despegando");
	}
	public void aterrizar(){
		JOptionPane.showMessageDialog(null, "La avioneta est� aterrizando");
	}
}
