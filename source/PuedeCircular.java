/**Interfaz
 * 
 * @author MariCreu
 *
 */
public interface PuedeCircular {
	public abstract String circular();
}
