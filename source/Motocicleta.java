import javax.swing.JOptionPane;

public class Motocicleta extends Vehiculo implements PuedeCircular {
	
	private int potenciaMotor;
	private boolean tieneMaletero;
	
	/**
	 * Constructores de motocicleta
	 */
	public Motocicleta() {
		super();
		setPotenciaMotor(50);
		setTieneMaletero(true);
	}
	public Motocicleta (String matricula, String marca,String modelo, String color, float kilometros, int numPuertas, int numPlazas, int potenciaMotor, boolean tieneMaletero){
		super(matricula, marca,modelo, color,kilometros,numPuertas, numPlazas);
		setPotenciaMotor(potenciaMotor);
		setTieneMaletero(tieneMaletero);
	}
	/** 
	 * Setters i Getters
	 * @return
	 */
	public int getPotenciaMotor() {
		return potenciaMotor;
	}

	public void setPotenciaMotor(int potenciaMotor) {
		this.potenciaMotor = potenciaMotor;
	}

	public boolean getTieneMaletero() {
		return tieneMaletero;
	}

	public void setTieneMaletero(boolean tieneMaletero) {
		this.tieneMaletero = tieneMaletero;
	}
/**
 * M�todos sobreescritos
 */
	@Override
	public String circular() {
		String circular;
		circular= "Esto es una moto y las motos pueden circular por carreteras, autov�as y autopistas";
		return circular;
	}

	@Override
	public String acelerar() {
		String acelerar;
		acelerar="La motocicleta est� acelerando";
		return acelerar;
	}

	@Override
	public String arrancar() {
		String arrancar;
		arrancar="La motocicleta est� arrancando";
		return arrancar;
	}

	@Override
	public String frenar() {
		String frenar;
		frenar="La motocicleta est� frenando";
		return frenar;
	}
	@Override
	public String toString(){
		String caracteristicas;
		String maletero;
		if (tieneMaletero){
			maletero="S�";
		}else{
			maletero="No";
		}
		
		caracteristicas= "\nLas Caracter�sticas de su " +getClass()+ " son:\n" + super.toString() + "\nPotencia del motor = "+getPotenciaMotor()+"\nMaletero= " + maletero;
		return caracteristicas;
	}
	/**
	 * M�todos propios de motocicleta
	 */
	public void brincar(){
		JOptionPane.showMessageDialog(null, "La moto est� brincando");
	}
	public void aparcar(){
		JOptionPane.showMessageDialog(null, "La moto est� aparcando");
	}

}