/**Interfaz
 * 
 * @author MariCreu
 *
 */
public interface PuedeVolar {
	public abstract String volar();
}
