
/**
 * Interfaz
 * @author MariCreu
 *
 */
public interface PuedeNavegar {
	public abstract String navegar();
}
